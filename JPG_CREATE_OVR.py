#!/usr/bin/python
#coding=utf-8
'''
    project=edu-test,
    name=overviewtest.py,
    date=2019/12/25 0025,
    author='zhangxuya',
    author_email='lionhenryzxy@sina.com',
    function：
    切片JPG构建金字塔OVR
    prerequisite:
        based on Python 3.x
        need Python module
    Any issues or improvements please contact author
'''
import gdal
import os
import glob
def create_overview(inputfile):
    if not os.path.exists(inputfile + '.ovr'):
        handle = gdal.Open(inputfile)
        if handle == None:
            print(r"文件打开错误")
        else:
            print(r'文件信息: {}列，{}行，{}波段'.format(handle.RasterXSize,handle.RasterYSize,handle.RasterCount))
            print(r'地理信息:',handle.GetGeoTransform())
            print(r'投影信息:',handle.GetProjectionRef())
            handle.BuildOverviews(overviewlist=[1,2,4,8])
            print(r'金字塔文件构建完成，{}'.format(inputfile + '.ovr'))
    else:
        print(r'{}文件已存在'.format(inputfile + '.ovr'))
if __name__ == '__main__':
    basedir = os.path.abspath(r'D:\pyproject\service-tile\20200210000000')#os.path.dirname(__file__)
    print(basedir)
    path = os.path.join(basedir,'*.JPG')
    filelist= glob.glob(path)
    for singlefile in filelist:
        create_overview(singlefile)
    print(r'转换完毕')
#     inputfile = sys.argv[1]
#     outputfile = sys.argv[2]
#     create_overview(inputfile,outputfile)
# a = Image.open("D:\DATA\FY3D_MERSI_GBAL_L2_PAD_MLT_GLL_20191219_POAD_004KM_MS_03_02_01.JPG")
# col,row = a.size
# b = a.resize((col*16,row*16),Image.NEAREST)
# b.save('D:\DATA\FY3D_MERSI_GBAL_L2_PAD_MLT_GLL_20191219_POAD_016KM_MS_03_02_01.JPG')