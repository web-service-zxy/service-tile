﻿#_*_ coding:utf-8-*-
import base64
from flask import Flask,request,Response
from gevent import monkey
from gevent.pywsgi import WSGIServer
from get_image_ult_path import get_tile_image, draw_tile_image, get_map_image, draw_map_image

monkey.patch_all()

app = Flask(__name__)


@app.route('/')
def hello_world():
    return 'Hello World!'
@app.route('/image')
def image():
    if request.method == 'GET':
        print('here')
        s1 = request.args.to_dict()
        print(s1)
        box = s1['BBOX']
        height = s1['HEIGHT']
        level = s1['LEVEL']
        width = s1['WIDTH']
        service = s1['SERVICE']
        product = s1['PRODUCT']
        starttime = s1['STARTTIME']
        if 'ENDTIME' in s1:
            print(s1['ENDTIME'])
        endtime = s1['ENDTIME']
        if service == 'UTTS' or service == 'WMTS':
            if 'COLORBAR' not in s1:
                pathname = get_tile_image(box, level, width, height, product, starttime)
                resp = Response(pathname, mimetype="image/jpeg")
            else:
                colorbar = s1['COLORBAR']
                pathname = draw_tile_image(box, level, width, height, product, starttime, base64.b64decode(colorbar))
                resp = Response(pathname, mimetype="image/jpeg")

        elif service == 'UTSS' or service == 'WMS':#wms
            if 'COLORBAR' not in s1:
                pathname = get_map_image(box, level, width, height, product, starttime, endtime)
                resp = Response(pathname, mimetype="image/png")
            else:
                colorbar = s1['COLORBAR']
                pathname = draw_map_image(box, level, width, height, product, starttime, endtime, base64.b64decode(colorbar))
                resp = Response(pathname, mimetype="image/png")
        resp.headers['Access-Control-Allow-Origin'] = '*'
        return resp
if __name__ == '__main__':
    http_server = WSGIServer(('0.0.0.0', 4550), app)
    print(r'服务启动，在{}的{}端口'.format('0.0.0.0', 4550))
    http_server.serve_forever()