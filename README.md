# 金字塔切片服务

程序以OVR作为切片相比以往传统切片方式，文件数量少了很多，减少了服务器的索引节点。

## 目录
- [目录](#目录)
- [程序文件说明](#程序文件说明)
- [请求头](#请求头)
- [请求方式](#请求方式)
- [参数](#参数)
- [切片层级说明](#切片层级说明)
- [两种切片服务说明](#两种切片服务说明)
- [调色板说明](#调色板说明)
- [两种切片服务说明](#两种切片服务说明)
    - [UTTS/WMTS](#UTTS/WMTS)
    - [UTSS/WMS](#UTSS/WMS)
- [产品名称说明](#产品名称说明)
- [文件路径配置说明](#文件路径配置说明)
- [请求示例](#请求示例)
- [返回结果](#返回结果)
    - [UTTS/WMTS](#UTTS/WMTS)
    - [UTSS/WMS](#UTSS/WMS)
    

## 程序文件说明
* 主程序：main_ult.py
* 图像处理程序：get_image_ult.py
* 绘图程序： drawer.py
* 单线程测试程序： main.py
* 处理hdf为规格的tif，构建金字塔：hdf_normal.py
* 图片规格大小处理程序： imag_normal.py
* 测试网页： test.html
* 生成tif时需要的投影生成程序： proj_name.py
* 文件路径配置文件： product_config.py

## 请求头  
"http://121.36.13.81:4550/image?"  
## 请求方式  
GET  
## 参数  
|参数|名称|示例|  
|:---:|:---:|:---:|  
|BOX|请求的经纬度范围|minlon,minlat,maxlon,maxlat|  
|HEIGHT|切片高度|256/512|  
|WIDTH|切片宽度|256/512|  
|LEVEL|切片所在层级|0-9|  
|SERVICE|服务类型|UTTS/WMTS,UTSS/WMS|  
|COLORBAR|调色板|json转的base64|
|PRODUCT|产品名称|FY4A_AGRI_SST_POAY_D|
|STARTTIME|开始时间|20200207201000|
|ENDTIME|结束时间（UTTS可以不传这个）|20200207201059|  

## 切片层级说明
切片分了0到9，一共十层。每层分辨率为  

|层级  |分辨率(°)|
|:---:|:---:|
|0|0.3515625|
|1|0.17578125|
|2|0.087890625|
|3|0.0439453125|
|4|0.02197265625|
|5|0.010986328125|
|6|0.0054931640625|
|7|0.00274658203125|
|8|0.001373291015625|
|9|0.0006866455078125|


## 两种切片服务说明
### UTTS/WMTS  
1. 针对切片文件，一张图片（一块）是11.25度
2. 分辨率为250m，需要扩到第七层，对应分辨率为0.00274658203125度，图像从4500*4500到4096*4096.
3. 构建金字塔只构建[1,2,4,8]四层对应7,6,5,4层展示
4. 当图像超过第七层时，还是使用第七层数据，进行重采样，图片会有一定的失真
5. 当需求为第4层时，0.02197265625*512，一张图片正好是11.25度，图像获取时正好不用跨块
6. 当0-3层时，需要跨块，替换为全球图，4km分辨率，需要扩到第三层，对应分辨率为0.0439453125度，图像从4500*8992到4096*8196,这里默认取名这张图片为all.jpg数据为all.tif
7. 构建金字塔只构建[1,2,4,8]四层对应3,2,1,0层展示
### UTSS/WMS  
1. 针对单图的文件
2. 不限制分辨率和图像的范围。

## 调色板说明
json格式
```json

	"range": [
		[
			[0, 11],
			[255, 252, 232, 0]
		],
		[
			[11, 15],
			[254, 226, 133]
		],
		[
			[15, 17],
			[253, 220, 104]
		],
		[
			[17, 19],
			[254, 174, 103]
		],
		[
			[19, 21],
			[254, 130, 63]
		],
		[
			[21, 23],
			[243, 88, 14]
		],
		[
			[23, 24],
			[219, 56, 14]
		]
	],
	"single": [
		[
			32767, [0, 0, 0, 0],
			"填充值"
		],
		[
			32000, [0, 0, 255, 255]
		],
		[
			32001, [255, 255, 255, 255]
		]
	],
	"gradient": [
		[-50, [255, 0, 0, 255]],
		[-28, [255, 100, 100, 255]],
		[-24, [255, 130, 130, 255]],
		[-20, [255, 150, 150, 255]],
		[-16, [255, 150, 100, 255]],
		[-12, [255, 200, 100, 255]],
		[-8, [255, 180, 100, 255]],
		[-4, [255, 170, 100, 255]],
		[
			0, [255, 240, 220, 255]
		],
		[
			4, [255, 240, 200, 255]
		],
		[
			8, [190, 255, 255, 255]
		],
		[
			12, [180, 255, 255, 255]
		],
		[
			16, [170, 255, 255, 255]
		],
		[
			20, [150, 255, 255, 255]
		],
		[
			28, [100, 255, 255, 255]
		],
		[
			38, [50, 255, 255, 255]
		],
		[
			48, [0, 255, 255, 255]
		],
		[
			60, [0, 204, 204, 255]
		],
		[
			80, [0, 153, 153, 255]
		],
		[
			100, [0, 102, 102, 255]
		]
	],
	"level": [],
	"name": "",
	"offset": 0.0,
	"slope": 1.0,
	"unit": "",
	"shape": "white",
	"defaultPalette": "gradient"
}
```
转换后的base64
```
JXU4RkQ5JXU5MUNDJXU2NjJGJXU4OTgxJXU1MkEwJXU1QkM2JXU3Njg0JXU1MTg1JXU1QkI5JXVGRjAxJTBBJTA5JTIycmFuZ2UlMjIlM0ElMjAlNUIlMEElMDklMDklNUIlMEElMDklMDklMDklNUIwJTJDJTIwMTElNUQlMkMlMEElMDklMDklMDklNUIyNTUlMkMlMjAyNTIlMkMlMjAyMzIlMkMlMjAwJTVEJTBBJTA5JTA5JTVEJTJDJTBBJTA5JTA5JTVCJTBBJTA5JTA5JTA5JTVCMTElMkMlMjAxNSU1RCUyQyUwQSUwOSUwOSUwOSU1QjI1NCUyQyUyMDIyNiUyQyUyMDEzMyU1RCUwQSUwOSUwOSU1RCUyQyUwQSUwOSUwOSU1QiUwQSUwOSUwOSUwOSU1QjE1JTJDJTIwMTclNUQlMkMlMEElMDklMDklMDklNUIyNTMlMkMlMjAyMjAlMkMlMjAxMDQlNUQlMEElMDklMDklNUQlMkMlMEElMDklMDklNUIlMEElMDklMDklMDklNUIxNyUyQyUyMDE5JTVEJTJDJTBBJTA5JTA5JTA5JTVCMjU0JTJDJTIwMTc0JTJDJTIwMTAzJTVEJTBBJTA5JTA5JTVEJTJDJTBBJTA5JTA5JTVCJTBBJTA5JTA5JTA5JTVCMTklMkMlMjAyMSU1RCUyQyUwQSUwOSUwOSUwOSU1QjI1NCUyQyUyMDEzMCUyQyUyMDYzJTVEJTBBJTA5JTA5JTVEJTJDJTBBJTA5JTA5JTVCJTBBJTA5JTA5JTA5JTVCMjElMkMlMjAyMyU1RCUyQyUwQSUwOSUwOSUwOSU1QjI0MyUyQyUyMDg4JTJDJTIwMTQlNUQlMEElMDklMDklNUQlMkMlMEElMDklMDklNUIlMEElMDklMDklMDklNUIyMyUyQyUyMDI0JTVEJTJDJTBBJTA5JTA5JTA5JTVCMjE5JTJDJTIwNTYlMkMlMjAxNCU1RCUwQSUwOSUwOSU1RCUwQSUwOSU1RCUyQyUwQSUwOSUyMnNpbmdsZSUyMiUzQSUyMCU1QiUwQSUwOSUwOSU1QiUwQSUwOSUwOSUwOTMyNzY3JTJDJTIwJTVCMCUyQyUyMDAlMkMlMjAwJTJDJTIwMCU1RCUyQyUwQSUwOSUwOSUwOSUyMiV1NTg2QiV1NTE0NSV1NTAzQyUyMiUwQSUwOSUwOSU1RCUyQyUwQSUwOSUwOSU1QiUwQSUwOSUwOSUwOTMyMDAwJTJDJTIwJTVCMCUyQyUyMDAlMkMlMjAyNTUlMkMlMjAyNTUlNUQlMEElMDklMDklNUQlMkMlMEElMDklMDklNUIlMEElMDklMDklMDkzMjAwMSUyQyUyMCU1QjI1NSUyQyUyMDI1NSUyQyUyMDI1NSUyQyUyMDI1NSU1RCUwQSUwOSUwOSU1RCUwQSUwOSU1RCUyQyUwQSUwOSUyMmdyYWRpZW50JTIyJTNBJTIwJTVCJTBBJTA5JTA5JTVCLTUwJTJDJTIwJTVCMjU1JTJDJTIwMCUyQyUyMDAlMkMlMjAyNTUlNUQlNUQlMkMlMEElMDklMDklNUItMjglMkMlMjAlNUIyNTUlMkMlMjAxMDAlMkMlMjAxMDAlMkMlMjAyNTUlNUQlNUQlMkMlMEElMDklMDklNUItMjQlMkMlMjAlNUIyNTUlMkMlMjAxMzAlMkMlMjAxMzAlMkMlMjAyNTUlNUQlNUQlMkMlMEElMDklMDklNUItMjAlMkMlMjAlNUIyNTUlMkMlMjAxNTAlMkMlMjAxNTAlMkMlMjAyNTUlNUQlNUQlMkMlMEElMDklMDklNUItMTYlMkMlMjAlNUIyNTUlMkMlMjAxNTAlMkMlMjAxMDAlMkMlMjAyNTUlNUQlNUQlMkMlMEElMDklMDklNUItMTIlMkMlMjAlNUIyNTUlMkMlMjAyMDAlMkMlMjAxMDAlMkMlMjAyNTUlNUQlNUQlMkMlMEElMDklMDklNUItOCUyQyUyMCU1QjI1NSUyQyUyMDE4MCUyQyUyMDEwMCUyQyUyMDI1NSU1RCU1RCUyQyUwQSUwOSUwOSU1Qi00JTJDJTIwJTVCMjU1JTJDJTIwMTcwJTJDJTIwMTAwJTJDJTIwMjU1JTVEJTVEJTJDJTBBJTA5JTA5JTVCJTBBJTA5JTA5JTA5MCUyQyUyMCU1QjI1NSUyQyUyMDI0MCUyQyUyMDIyMCUyQyUyMDI1NSU1RCUwQSUwOSUwOSU1RCUyQyUwQSUwOSUwOSU1QiUwQSUwOSUwOSUwOTQlMkMlMjAlNUIyNTUlMkMlMjAyNDAlMkMlMjAyMDAlMkMlMjAyNTUlNUQlMEElMDklMDklNUQlMkMlMEElMDklMDklNUIlMEElMDklMDklMDk4JTJDJTIwJTVCMTkwJTJDJTIwMjU1JTJDJTIwMjU1JTJDJTIwMjU1JTVEJTBBJTA5JTA5JTVEJTJDJTBBJTA5JTA5JTVCJTBBJTA5JTA5JTA5MTIlMkMlMjAlNUIxODAlMkMlMjAyNTUlMkMlMjAyNTUlMkMlMjAyNTUlNUQlMEElMDklMDklNUQlMkMlMEElMDklMDklNUIlMEElMDklMDklMDkxNiUyQyUyMCU1QjE3MCUyQyUyMDI1NSUyQyUyMDI1NSUyQyUyMDI1NSU1RCUwQSUwOSUwOSU1RCUyQyUwQSUwOSUwOSU1QiUwQSUwOSUwOSUwOTIwJTJDJTIwJTVCMTUwJTJDJTIwMjU1JTJDJTIwMjU1JTJDJTIwMjU1JTVEJTBBJTA5JTA5JTVEJTJDJTBBJTA5JTA5JTVCJTBBJTA5JTA5JTA5MjglMkMlMjAlNUIxMDAlMkMlMjAyNTUlMkMlMjAyNTUlMkMlMjAyNTUlNUQlMEElMDklMDklNUQlMkMlMEElMDklMDklNUIlMEElMDklMDklMDkzOCUyQyUyMCU1QjUwJTJDJTIwMjU1JTJDJTIwMjU1JTJDJTIwMjU1JTVEJTBBJTA5JTA5JTVEJTJDJTBBJTA5JTA5JTVCJTBBJTA5JTA5JTA5NDglMkMlMjAlNUIwJTJDJTIwMjU1JTJDJTIwMjU1JTJDJTIwMjU1JTVEJTBBJTA5JTA5JTVEJTJDJTBBJTA5JTA5JTVCJTBBJTA5JTA5JTA5NjAlMkMlMjAlNUIwJTJDJTIwMjA0JTJDJTIwMjA0JTJDJTIwMjU1JTVEJTBBJTA5JTA5JTVEJTJDJTBBJTA5JTA5JTVCJTBBJTA5JTA5JTA5ODAlMkMlMjAlNUIwJTJDJTIwMTUzJTJDJTIwMTUzJTJDJTIwMjU1JTVEJTBBJTA5JTA5JTVEJTJDJTBBJTA5JTA5JTVCJTBBJTA5JTA5JTA5MTAwJTJDJTIwJTVCMCUyQyUyMDEwMiUyQyUyMDEwMiUyQyUyMDI1NSU1RCUwQSUwOSUwOSU1RCUwQSUwOSU1RCUyQyUwQSUwOSUyMmxldmVsJTIyJTNBJTIwJTVCJTVEJTJDJTBBJTA5JTIybmFtZSUyMiUzQSUyMCUyMiUyMiUyQyUwQSUwOSUyMm9mZnNldCUyMiUzQSUyMDAuMCUyQyUwQSUwOSUyMnNsb3BlJTIyJTNBJTIwMS4wJTJDJTBBJTA5JTIydW5pdCUyMiUzQSUyMCUyMiUyMiUyQyUwQSUwOSUyMnNoYXBlJTIyJTNBJTIwJTIyd2hpdGUlMjIlMkMlMEElMDklMjJkZWZhdWx0UGFsZXR0ZSUyMiUzQSUyMCUyMmdyYWRpZW50JTIyJTBBJTdE
```
## 产品名称说明
* 示例：
FY4A_AGRI_SST_POAY_D  
* 组成
    - 卫星名称：FY4A/FY3D...
    - 仪器名称：AGRI/MERSI...
    - 产品名称：SST/NVI
    - 时次类别：POA*(时次:I,日:D,候:F,旬:T,月:M,年:Y)  
    - 时段标识：日/升轨:D, 夜/降轨:N  

程序会根据产品名称去product_config.py寻找对应路径，其中UTTS/WMTS服务会找到对应的文件夹路径，根据图片所提供的范围参数来寻找对应编号切片；UTSS/WMS则可以直接确定对应的文件名称和路径

## 请求示例  
http://121.36.13.81:4550/image?BBOX=0,0,90,90&LEVEL=1&WIDTH=512&HEIGHT=512&SERVICE=WMTS&COLORBAR=ew0KCSJyYW5nZSI6IFsNCgkJWw0KCQkJWzAsIDExXSwNCgkJCVsyNTUsIDI1MiwgMjMyLCAwXQ0KCQldLA0KCQlbDQoJCQlbMTEsIDE1XSwNCgkJCVsyNTQsIDIyNiwgMTMzXQ0KCQldLA0KCQlbDQoJCQlbMTUsIDE3XSwNCgkJCVsyNTMsIDIyMCwgMTA0XQ0KCQldLA0KCQlbDQoJCQlbMTcsIDE5XSwNCgkJCVsyNTQsIDE3NCwgMTAzXQ0KCQldLA0KCQlbDQoJCQlbMTksIDIxXSwNCgkJCVsyNTQsIDEzMCwgNjNdDQoJCV0sDQoJCVsNCgkJCVsyMSwgMjNdLA0KCQkJWzI0MywgODgsIDE0XQ0KCQldLA0KCQlbDQoJCQlbMjMsIDI0XSwNCgkJCVsyMTksIDU2LCAxNF0NCgkJXQ0KCV0sDQoJInNpbmdsZSI6IFsNCgkJWw0KCQkJMzI3NjcsIFswLCAwLCAwLCAwXSwNCgkJCSLloavlhYXlgLwiDQoJCV0sDQoJCVsNCgkJCTMyMDAwLCBbMCwgMCwgMjU1LCAyNTVdDQoJCV0sDQoJCVsNCgkJCTMyMDAxLCBbMjU1LCAyNTUsIDI1NSwgMjU1XQ0KCQldDQoJXSwNCgkiZ3JhZGllbnQiOiBbDQoJCVstNTAsIFsyNTUsIDAsIDAsIDI1NV1dLA0KCQlbLTI4LCBbMjU1LCAxMDAsIDEwMCwgMjU1XV0sDQoJCVstMjQsIFsyNTUsIDEzMCwgMTMwLCAyNTVdXSwNCgkJWy0yMCwgWzI1NSwgMTUwLCAxNTAsIDI1NV1dLA0KCQlbLTE2LCBbMjU1LCAxNTAsIDEwMCwgMjU1XV0sDQoJCVstMTIsIFsyNTUsIDIwMCwgMTAwLCAyNTVdXSwNCgkJWy04LCBbMjU1LCAxODAsIDEwMCwgMjU1XV0sDQoJCVstNCwgWzI1NSwgMTcwLCAxMDAsIDI1NV1dLA0KCQlbDQoJCQkwLCBbMjU1LCAyNDAsIDIyMCwgMjU1XQ0KCQldLA0KCQlbDQoJCQk0LCBbMjU1LCAyNDAsIDIwMCwgMjU1XQ0KCQldLA0KCQlbDQoJCQk4LCBbMTkwLCAyNTUsIDI1NSwgMjU1XQ0KCQldLA0KCQlbDQoJCQkxMiwgWzE4MCwgMjU1LCAyNTUsIDI1NV0NCgkJXSwNCgkJWw0KCQkJMTYsIFsxNzAsIDI1NSwgMjU1LCAyNTVdDQoJCV0sDQoJCVsNCgkJCTIwLCBbMTUwLCAyNTUsIDI1NSwgMjU1XQ0KCQldLA0KCQlbDQoJCQkyOCwgWzEwMCwgMjU1LCAyNTUsIDI1NV0NCgkJXSwNCgkJWw0KCQkJMzgsIFs1MCwgMjU1LCAyNTUsIDI1NV0NCgkJXSwNCgkJWw0KCQkJNDgsIFswLCAyNTUsIDI1NSwgMjU1XQ0KCQldLA0KCQlbDQoJCQk2MCwgWzAsIDIwNCwgMjA0LCAyNTVdDQoJCV0sDQoJCVsNCgkJCTgwLCBbMCwgMTUzLCAxNTMsIDI1NV0NCgkJXSwNCgkJWw0KCQkJMTAwLCBbMCwgMTAyLCAxMDIsIDI1NV0NCgkJXQ0KCV0sDQoJImxldmVsIjogW10sDQoJIm5hbWUiOiAiIiwNCgkib2Zmc2V0IjogMC4wLA0KCSJzbG9wZSI6IDEuMCwNCgkidW5pdCI6ICIiLA0KCSJzaGFwZSI6ICJ3aGl0ZSIsDQoJImRlZmF1bHRQYWxldHRlIjogImdyYWRpZW50Ig0KfQ==

## 返回结果  
### UTTS/WMTS  
* 有colorbar时：取原始数据，根据调色板绘制作为切片返回jpeg  
* 无colorbar时：取原始图像部分作为切片返回jpeg
* 错误时：返回空白图片jpeg
### UTSS/WMS  
* 有colorbar时：取原始数据，根据调色板绘制作为切片返回png  
* 无colorbar时：取原始图像部分作为切片返回png  
* 错误时：返回空白图片png 

## 文件路径配置说明
### UTTS
* 因为切片一般都是jpg的,所以我觉得jpg和tif文件可以放在一起，用一个文件配置路径可以，我在有colorbar和无colorbar服务中分别设定找jpg和tif就行.
* product_config.py中的UTTS字典中添加配置就可以了
* key是产品名称，value是路径，服务程序回去路径下找对应编号的切片进行贴图
* YYYYmmddHHMMSS对应的是starttime参数中的年月日时分秒  
* 示例：
```
    UTTS_PATH = {
            'test':"/SatGroupA/TESTDATA/TILEDATA/YYYYmmddHHMMSS",
            'FY4A_AGRI_SST_POAY_D':"/SatGroupA/TESTDATA/TILEDATA/YYYYmmddHHMMSS"
    } 
```
### UTSS
* 除了TIF外，因为考虑到单图文件作为切片时会出现很多空白区域采用透明色，所以就默认为PNG存储，但是不敢确定是否会存储成为jpg，所以想把是jpg还是png的决定权放在路径配置中，这样就不得不把一个产品有无colorbar的两个服务对应的路径分开来设置，当然确认只有jpg或者png后可以改成类似UTTS一样合成一个路径。
* 'D'的字典中存放是有colorbar服务对应找的路径，'G'的字典中存放是无colorbar服务对应找的路径
* YYYYmmddHHMMSS对应的是starttime参数中的年月日时分秒  
* %Y%m%d%H%M%S对应的是endtime参数中的年月日时分秒  
示例
```
    UTSS_PATH = {
        'G':
            {
                'test':"/SatGroupA/TESTDATA/TILEDATA/YYYYmmddHHMMSS_%Y%m%d%H%M%S.png",
                'FY4A_AGRI_SST_POAY_D':"/SatGroupA/TESTDATA/TILEDATA/YYYYmmddHHMMSS_%Y%m%d%H%M%S.png"
            },
        'D':
            {
                'test':"/SatGroupA/TESTDATA/TILEDATA/YYYYmmddHHMMSS_%Y%m%d%H%M%S.tif",
                'FY4A_AGRI_SST_POAY_D':"/SatGroupA/TESTDATA/TILEDATA/YYYYmmddHHMMSS_%Y%m%d%H%M%S.tif"
            }
    }
```
