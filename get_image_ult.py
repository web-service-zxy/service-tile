﻿#_*_ coding:utf-8-*-
import gdal
import os
from io import BytesIO
from PIL import Image
from decimal import Decimal
import numpy as np
import base64
from datetime import datetime,timedelta
from produc_config import UTTS_PATH, UTSS_PATH
from drawer import Drawer
'''gdal test
'''
def calendtime(starttime, timeflag):
    '''
    根据开始时间和时间标识计算结束时间
    :param starttime:
    :param timeflag:
    :return:
    '''
    if timeflag == 'Y':
        endtime = datetime(starttime.year,12,31,23,59,59)
    elif timeflag == 'M':
        next_month = starttime.replace(day=28) + datetime.timedelta(days=4)  # this will never fail
        last_day = next_month - datetime.timedelta(days=next_month.day)
        endtime = last_day.replace(hour=23,minute=59,second=59)
    elif timeflag == 'T':
        endtime = datetime(starttime.year,starttime.month,starttime.day+9,23,59,59)
    elif timeflag == 'F':
        endtime = datetime(starttime.year, starttime.month, starttime.day + 4 , 23, 59, 59)
    elif timeflag == 'D':
        endtime = starttime.replace(hour=23,minute=59,second=59)
    return endtime


def timematch(product, stimestr, servicename, method, etimestr='0'):
    '''根据传入的路径模板和时间进行匹配
    '''
    starttime = datetime.strptime(stimestr, '%Y%m%d%H%M%S')
    if servicename == 'UTTS':
        path = UTTS_PATH[product]
    elif servicename == 'UTSS':
        # timeflag = product.split('_')[3][-1]
        # endtime = calendtime(starttime, timeflag)
        path = UTSS_PATH[method][product]
        endtime = datetime.strptime(etimestr, '%Y%m%d%H%M%S')
        path = path.replace("%Y", str(endtime.year).zfill(4))
        path = path.replace('%m', str(endtime.month).zfill(2))
        path = path.replace('%d', str(endtime.day).zfill(2))
        path = path.replace('%H', str(endtime.hour).zfill(2))
        path = path.replace('%M', str(endtime.minute).zfill(2))
        path = path.replace('%S', str(endtime.second).zfill(2))
    path = path.replace("YYYY",str(starttime.year).zfill(4))
    path = path.replace('mm',str(starttime.month).zfill(2))
    path = path.replace('dd', str(starttime.day).zfill(2))
    path = path.replace('HH', str(starttime.hour).zfill(2))
    path = path.replace('MM', str(starttime.minute).zfill(2))
    path = path.replace('SS', str(starttime.second).zfill(2))
    return path

RES = [0.3515625, 0.17578125, 0.087890625, 0.0439453125, 0.02197265625, 0.010986328125, 0.0054931640625, 0.00274658203125, 0.001373291015625, 0.0006866455078125]

def get_tile_image(box, z, width, height, product, time):
    try:
        print(r'我进来了,Get_tile_image')
        #获取图像所在文件路径
        path = timematch(product, time, 'UTTS', 'G')
        print(r'文件所在路径为{}'.format(path))
        #获取请求图像经纬度
        box = [float(x) for x in box.split(',')]
        image_minlon = box[0]
        image_minlat = box[1]
        image_maxlon = box[2]
        image_maxlat = box[3]
        if int(z) < 4:
            print(r'等级{}，采用低分辨率原图'.format(z))
            # 拼出文件名，获取图像文件和图片的左上角经纬度
            imagepath = os.path.join(path, 'all.JPG')
            tile_ltlon = -180
            tile_ltlat = 90
            # 获取层数和分辨率
            floor = max(3 - int(z), 0)
            print(r'使用', floor, r'层数据')
        else:
            #计算图像所在的切片号和切片的左上角经纬度
            col = min(int((image_maxlon - (-180)) / 11.25),int((image_minlon - (-180)) / 11.25))
            row = min(int((image_maxlat - (-90)) / 11.25),int((image_minlat - (-90)) / 11.25))
            print(r'{}行{}列'.format(row,col))
            num = row * 32 + col
            print(r'使用第{}块数据'.format(num))
            tile_ltlon = -180 + col * 11.25
            tile_ltlat = (row + 1) * 11.25 + (-90)
            print(r'左上角经度{}，纬度{}'.format(tile_ltlon,tile_ltlat))
            # 拼出文件名，获取图像文件
            imagepath = os.path.join(path, str(num).zfill(3) + '.JPG')
            # 获取层数
            floor = max(7 - int(z), 0)
            print(r'使用', floor, r'层数据')
        print(r'使用数据{}'.format(imagepath))
        if not os.path.exists(imagepath):
            return None
        gdal.AllRegister()
        ds = gdal.Open(imagepath)
        print(ds)
        #获取分辨率
        tilepre = RES[min(int(z),7)]
        print(r'分辨率为{}'.format(tilepre))

        #获取金字塔中的起始行列号和行列数
        overview_startcol = int(round(Decimal(str((image_minlon - tile_ltlon) / tilepre))))
        overview_startrow = int(round(Decimal(str((tile_ltlat - image_maxlat) / tilepre))))
        overview_width = int(round(Decimal(str(image_maxlon - image_minlon)) / Decimal(tilepre))) #256
        overview_height = int(round(Decimal(str(image_maxlat - image_minlat)) / Decimal(tilepre))) #256
        print(r'原始图片的{}行到{}行，{}列到{}列'.format(overview_startrow, overview_startrow + overview_height-1, overview_startcol, overview_startcol + overview_width -1))

        #定义要生成的图片大小
        width = int(width)
        height = int(height)

        #判断是否有金字塔，没有就生成
        if not os.path.exists(imagepath+'.ovr'):
            ds.BuildOverviews(overviewlist=[1, 2, 4, 8])

        #读取金字塔数据
        result = np.empty((height, width, ds.RasterCount), dtype=np.uint8)
        for i in range(1,ds.RasterCount+1):
            band = ds.GetRasterBand(i)
            ovr = band.GetOverview(floor)
            data = ovr.ReadAsArray(overview_startcol, overview_startrow, overview_width, \
                                   overview_height, width, height, band.DataType)
            result[:,:,i-1] = data

        #转成base64
        img_buffer = BytesIO()
        newimage = Image.fromarray(result)
        newimage.save(img_buffer,format='JPEG')
        byte_data = img_buffer.getvalue()
        print(r'完事了')
        return byte_data

    except Exception as err:
        print(err)
        #生成空白图片
        result = np.full((height,width,ds.RasterCount),255,dtype=np.uint8)
        # 转成base64
        img_buffer = BytesIO()
        newimage = Image.fromarray(result)
        newimage.save(img_buffer, format='JPEG')
        byte_data = img_buffer.getvalue()
        print(r'错误了，返回空白图片')
        return byte_data

def draw_tile_image(box, z, width, height, product, time, colorbar):
    try:
        print(r'我进来了,draw_tile_image')
        # 获取图像所在文件路径
        path = timematch(product, time, 'UTTS', 'D')
        print(r'文件所在路径为{}'.format(path))
        #获取请求图像经纬度
        box = [float(x) for x in box.split(',')]
        image_minlon = box[0]
        image_minlat = box[1]
        image_maxlon = box[2]
        image_maxlat = box[3]
        if int(z) < 4:
            print(r'等级{}，采用低分辨率原图'.format(z))
            # 拼出文件名，获取图像文件和图片的左上角经纬度
            imagepath = os.path.join(path, 'all.tif')
            tile_ltlon = -180
            tile_ltlat = 90
            # 获取层数和分辨率
            floor = max(3 - int(z), 0)
            print(r'使用', floor, r'层数据')
        else:
            #计算图像所在的切片号和切片的左上角经纬度
            col = min(int((image_maxlon - (-180)) / 11.25),int((image_minlon - (-180)) / 11.25))
            row = min(int((image_maxlat - (-90)) / 11.25),int((image_minlat - (-90)) / 11.25))
            print(r'{}行{}列'.format(row,col))
            num = row * 32 + col
            print(r'使用第{}块数据'.format(num))
            tile_ltlon = -180 + col * 11.25
            tile_ltlat = (row + 1) * 11.25 + (-90)
            print(r'左上角经度{}，纬度{}'.format(tile_ltlon,tile_ltlat))
            # 拼出文件名，获取图像文件
            imagepath = os.path.join(path, str(num).zfill(3) + '.tif')
            # 获取层数
            floor = max(7 - int(z), 0)
            print(r'使用', floor, r'层数据')
        print(r'使用数据{}'.format(imagepath))
        if not os.path.exists(imagepath):
            return None
        gdal.AllRegister()
        ds = gdal.Open(imagepath)

        #获取分辨率
        tilepre = RES[min(int(z),7)]
        print(r'分辨率为{}'.format(tilepre))

        #获取金字塔中的起始行列号和行列数
        overview_startcol = int(round(Decimal(str((image_minlon - tile_ltlon) / tilepre))))
        overview_startrow = int(round(Decimal(str((tile_ltlat - image_maxlat) / tilepre))))
        overview_width = int(round(Decimal(str(image_maxlon - image_minlon)) / Decimal(tilepre))) #256
        overview_height = int(round(Decimal(str(image_maxlat - image_minlat)) / Decimal(tilepre))) #256
        print(r'原始图片的{}行到{}行，{}列到{}列'.format(overview_startrow, overview_startrow + overview_height-1, overview_startcol, overview_startcol + overview_width -1))

        #定义要生成的图片大小
        width = int(width)
        height = int(height)

        #判断是否有金字塔，没有就生成
        if not os.path.exists(imagepath+'.ovr'):
            ds.BuildOverviews(overviewlist=[1, 2, 4, 8])

        #读取金字塔数据
        # for i in range(1, ds.RasterCount + 1):
        for i in range(1,2):
            band = ds.GetRasterBand(i)
            ovr = band.GetOverview(floor)
            data = ovr.ReadAsArray(overview_startcol, overview_startrow, overview_width, \
                                   overview_height, width, height, band.DataType)
        print('数据是', data)
        drawer = Drawer(colorbar)
        imagedata = drawer.drawarray(data)
        imagedata = imagedata[:,:,:3]
        # 转成base64
        img_buffer = BytesIO()
        newimage = Image.fromarray(imagedata)
        newimage.save(img_buffer, format='JPEG')
        byte_data = img_buffer.getvalue()
        print(r'完事了')
        return byte_data

    except Exception as err:
        print(err)
        #生成空白图片
        result = np.full((height,width,ds.RasterCount),255,dtype=np.uint8)
        # 转成base64
        img_buffer = BytesIO()
        newimage = Image.fromarray(result)
        newimage.save(img_buffer, format='JPEG')
        byte_data = img_buffer.getvalue()
        print(r'错误了，返回空白图片')
        return byte_data

def min_box(tar_top_lat, tar_btm_lon, tar_btm_lat, tar_top_lon, src_top_lat, src_btm_lon, src_btm_lat, src_top_lon):
    '''计算最小box'''
    in_top_lon = tar_top_lon if tar_top_lon > src_top_lon and tar_top_lon < src_btm_lon else src_top_lon
    in_top_lat = tar_top_lat if tar_top_lat < src_top_lat else src_top_lat
    in_btm_lon = tar_btm_lon if tar_btm_lon < src_btm_lon else src_btm_lon
    in_btm_lat = tar_btm_lat if tar_btm_lat > src_btm_lat else src_btm_lat
    return in_top_lon, in_top_lat, in_btm_lon, in_btm_lat

def getindex(reslist,number):
    for index in range(len(reslist)):
        if number > reslist[index]:
            return index - 1,reslist[index-1]
        elif number == reslist[index]:
            return index,reslist[index]
    return index,reslist[index]

def get_map_image(box,z,width, height, product, starttime, endtime):
    try:
        print(r'我进来了,get_map_image')
        # 获取请求图像经纬度
        box = [float(x) for x in box.split(',')]
        image_minlon = box[0]
        image_minlat = box[1]
        image_maxlon = box[2]
        image_maxlat = box[3]
        print(r'等级{}'.format(z))

         # 拼出文件名，获取图像文件和四角经纬度信息
        # imagepath = os.path.join(path, "FY4A_AGRI_Latlon_L2_VWI_NULL_20191224024500_20191224025959_1000M_POAI_X.PNG.ovr")
        # ovrpath = os.path.join(path, "FY4A_AGRI_Latlon_L2_VWI_NULL_20191224024500_20191224025959_1000M_POAI_X.PNG.ovr")
        imagepath = timematch(product, starttime, 'UTSS','G',endtime)
        print(r'使用图像{}'.format(imagepath))
        gdal.AllRegister()
        ds = gdal.Open(imagepath)
        # ovrds = gdal.Open(ovrpath)
        col = ds.RasterXSize
        row = ds.RasterYSize
        geotrans = ds.GetGeoTransform()
        tile_ltlon, tile_ltlat, res = geotrans[0], geotrans[3], geotrans[1]
        index, tartileres = getindex(RES,res)
        tile_rblon = tile_ltlon + (col - 1) * res
        tile_rblat = tile_ltlat - (row - 1) * res
        print(r'图像左上角经度{}，纬度{}，右下角经度{}，纬度{}，分辨率{}，接近第{}层, 层分辨率{}'.format(tile_ltlon, tile_ltlat, tile_rblon, tile_rblat, res,
                                                                index,tartileres))

        # 获取层数和分辨率
        floor = max(index - int(z), 0)
        print(r'使用{}层数据'.format(floor))

        # 获取分辨率
        targetres = res * (2 ** floor)
        print(r'原图分辨率为{}'.format(targetres))
        tilepre = RES[int(z)]
        print(r'分辨率为{}'.format(tilepre))

        #获取最小区域
        in_top_lon, in_top_lat, in_btm_lon, in_btm_lat = min_box(image_maxlat, image_maxlon, image_minlat, image_minlon, tile_ltlat, tile_rblon, tile_rblat, tile_ltlon)

        # 获取金字塔中的起始行列号和行列数
        overview_startcol = int(round(Decimal(str((in_top_lon - tile_ltlon) / targetres))))
        overview_startrow = int(round(Decimal(str((tile_ltlat - in_top_lat) / targetres))))
        overview_width = int(round(Decimal(str(in_btm_lon - in_top_lon)) / Decimal(targetres)))  # 256
        overview_height = int(round(Decimal(str(in_top_lat - in_btm_lat)) / Decimal(targetres)))  # 256
        print(r'原始图片的{}行到{}行，{}列到{}列'.format(overview_startrow, overview_startrow + overview_height - 1,
                                            overview_startcol, overview_startcol + overview_width - 1))

        #获取最小区域在切边结果中的位置
        image_startcol = int(round(Decimal(str((in_top_lon - image_minlon) / tilepre))))
        image_startrow = int(round(Decimal(str((image_maxlat - in_top_lat) / tilepre))))
        image_width = int(round(Decimal(str(in_btm_lon - in_top_lon)) / Decimal(tilepre)))  # 256
        image_height = int(round(Decimal(str(in_top_lat - in_btm_lat)) / Decimal(tilepre)))  # 256
        print(r'生成图片的{}行到{}行，{}列到{}列'.format(image_startrow, image_startrow + image_height - 1,
                                            image_startcol, image_startcol + image_width - 1))
        # 定义要生成的图片大小
        width = int(width)
        height = int(height)

        # 读取金字塔数据
        result = np.empty((height, width,4), dtype=np.uint8)
        for i in range(1, ds.RasterCount + 1):
            band = ds.GetRasterBand(i)
            if floor == 0:
                data = band.ReadAsArray(overview_startcol, overview_startrow, overview_width, \
                                       overview_height, image_width,image_height, band.DataType)
            else:
                ovr = band.GetOverview(floor)
                data = ovr.ReadAsArray(overview_startcol, overview_startrow, overview_width, \
                                       overview_height, image_width,image_height, band.DataType)
            result[image_startrow:image_startrow + image_height, image_startcol:image_startcol + image_width,
            i-1] = data
        # 转成base64
        img_buffer = BytesIO()
        newimage = Image.fromarray(result)
        newimage.save(img_buffer, format='PNG')
        byte_data = img_buffer.getvalue()
        return byte_data

    except Exception as err:
        print(err)
        # 定义要生成的图片大小
        width = int(width)
        height = int(height)
        # 生成空白图片
        result = np.full((height, width, 4), 0, dtype=np.uint8)
        # 转成base64
        img_buffer = BytesIO()
        newimage = Image.fromarray(result)
        newimage.save(img_buffer, format='PNG')
        byte_data = img_buffer.getvalue()
        print(r'错误了，返回空白图片')
        return byte_data

def draw_map_image(box, z, width, height, product, starttime, endtime,colorbar):
    try:
        print(r'我进来了,draw_map_image')
        # 获取请求图像经纬度
        box = [float(x) for x in box.split(',')]
        image_minlon = box[0]
        image_minlat = box[1]
        image_maxlon = box[2]
        image_maxlat = box[3]
        print(r'等级{}'.format(z))
        # 拼出文件名，获取图像文件和四角经纬度信息,这里传tif文件是因为如果传ovr文件第0层是原始数据的第1层
        # imagepath = os.path.join(path, "FY4A_AGRI_Latlon_L2_VWI_NULL_20191224024500_20191224025959_1000M_POAI_X.tif")
        imagepath = timematch(product, starttime, 'UTSS', 'D', endtime)
        print(r'使用图像{}'.format(imagepath))
        gdal.AllRegister()
        ds = gdal.Open(imagepath)
        col = ds.RasterXSize
        row = ds.RasterYSize
        geotrans = ds.GetGeoTransform()
        tile_ltlon,tile_ltlat,res = geotrans[0],geotrans[3],geotrans[1]
        index,tartileres = getindex(RES,res)
        tile_rblon = tile_ltlon + (col-1) * res
        tile_rblat = tile_ltlat - (row -1) * res
        print(r'图像左上角经度{}，纬度{}，右下角经度{}，纬度{}，分辨率{}，接近第{}层，层分辨率{}'.format(tile_ltlon,tile_ltlat,tile_rblon, tile_rblat, res, index, tartileres))

        # 获取层数和分辨率
        floor = max(index - int(z), 0)
        print(r'使用{}层数据'.format(floor))

        # 获取分辨率
        targetres = res * (2 ** floor)
        print(r'原图分辨率为{}'.format(targetres))
        tilepre = RES[int(z)]
        print(r'分辨率为{}'.format(tilepre))

        #获取最小区域
        in_top_lon, in_top_lat, in_btm_lon, in_btm_lat = min_box(image_maxlat, image_maxlon, image_minlat, image_minlon, tile_ltlat, tile_rblon, tile_rblat, tile_ltlon)

        # 获取金字塔中的起始行列号和行列数
        overview_startcol = int(round(Decimal(str((in_top_lon - tile_ltlon) / targetres))))
        overview_startrow = int(round(Decimal(str((tile_ltlat - in_top_lat) / targetres))))
        overview_width = int(round(Decimal(str(in_btm_lon - in_top_lon)) / Decimal(targetres)))  # 256
        overview_height = int(round(Decimal(str(in_top_lat - in_btm_lat)) / Decimal(targetres)))  # 256
        print(r'原始图片的{}行到{}行，{}列到{}列'.format(overview_startrow, overview_startrow + overview_height - 1,
                                            overview_startcol, overview_startcol + overview_width - 1))

        #获取最小区域在切边结果中的位置
        image_startcol = int(round(Decimal(str((in_top_lon - image_minlon) / tilepre))))
        image_startrow = int(round(Decimal(str((image_maxlat - in_top_lat) / tilepre))))
        image_width = int(round(Decimal(str(in_btm_lon - in_top_lon)) / Decimal(tilepre)))  # 256
        image_height = int(round(Decimal(str(in_top_lat - in_btm_lat)) / Decimal(tilepre)))  # 256
        print(r'生成图片的{}行到{}行，{}列到{}列'.format(image_startrow, image_startrow + image_height - 1,
                                            image_startcol, image_startcol + image_width - 1))
        # 定义要生成的图片大小
        width = int(width)
        height = int(height)

        # 读取金字塔数据
        result = np.empty((height, width,4), dtype=np.uint8)
        for i in range(1, ds.RasterCount + 1):
            band = ds.GetRasterBand(i)
            ovr = band.GetOverview(floor)
            data = ovr.ReadAsArray(overview_startcol, overview_startrow, overview_width, \
                                   overview_height, image_width,image_height, band.DataType)
        print(r'数据是',data)

        #图像上册
        print(colorbar)
        drawer = Drawer(colorbar)
        imagedata = drawer.drawarray(data)

        #图像镶嵌
        result[image_startrow:image_startrow + image_height,image_startcol:image_startcol + image_width,:] = imagedata

        # 转成byte型
        img_buffer = BytesIO()
        newimage = Image.fromarray(result)
        newimage.save(img_buffer, format='PNG')
        byte_data = img_buffer.getvalue()
        print(r'完事了')
        return byte_data

    except Exception as err:
        print(err)
        # 生成空白图片
        result = np.full((height, width, 4), 0, dtype=np.uint8)
        # 转成base64
        img_buffer = BytesIO()
        newimage = Image.fromarray(result)
        newimage.save(img_buffer, format='PNG')
        byte_data = img_buffer.getvalue()
        print(r'错误了，返回空白图片')
        return byte_data


if __name__ == '__main__':
    #最小经度，最小纬度，最大经度，最大纬度
    get_tile_image('-180,78.75,-168.75,90',1, 256, 256, r'D:\DATA\tile')
