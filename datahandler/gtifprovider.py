# coding=utf-8
"""
create by fantasylin 2018/11/27
"""
from decimal import Decimal
import numpy as np
from osgeo import osr
import gdal
# from customerr.cus_error import BandError

class GTIFProvider(object):
    """
    GEOTiff文件读取类
    """

    def __init__(self, filename):
        self.gtif_obj = gdal.Open(filename)
        self.col = self.gtif_obj.RasterXSize
        self.row = self.gtif_obj.RasterYSize
        self.bands = self.gtif_obj.RasterCount
        self.geotrans = self.gtif_obj.GetGeoTransform()

    def get_dataset(self, band=0):
        """
        获取数据集
        """
        band = int(band)
        dataset = np.zeros([self.row, self.col, self.bands])
        for i in range(self.bands):
            tmp_set = self.gtif_obj.GetRasterBand(1)
            dataset[:, :, i] = tmp_set.ReadAsArray(0, 0, self.col, self.row)
        tmp_len = len(dataset[:, :, band])
        if tmp_len == 0:
            raise BandError()
        return dataset[:, :, band]

        # tmp_set = self.gtif_obj.ReadAsArray(0, 0, self.col, self.row)
        # dataset = tmp_set[0:self.row, 0:self.col]
        # dataset_len = len(dataset)
        # if dataset_len:
        #     raise BandError(band)
        # else:
        #     return dataset

    @property
    def box(self):
        """
        经纬度范围
        """
        toplon, toplat, res = self.geotrans[0], self.geotrans[3], self.geotrans[1]
        max_lon = Decimal(str(toplon)) + Decimal(str((self.col - 0.5))) * Decimal(str(res))
        max_lat = toplat
        min_lon = toplon
        min_lat = Decimal(str(toplat)) - Decimal(str((self.row - 0.5))) * Decimal(str(res))
        return [Decimal(str(max_lat)), Decimal(str(max_lon)), Decimal(str(min_lat)), Decimal(str(min_lon))]

    @property
    def precision(self):
        """
        分辨率
        """
        lat_precision = self.geotrans[1]
        # lon_precision = self.geotrans[1]
        lat_precision = Decimal(str(lat_precision))
        # lon_precision = Decimal(str(lon_precision))
        # return [lat_precision, lon_precision]
        return lat_precision
    @property
    def imgsize(self):
        """图像尺寸[w, h]"""
        return [self.col, self.row]

    @property
    def projection(self):
        """投影方式"""
        projref = self.gtif_obj.GetProjectionRef()
        srs = osr.SpatialReference()
        srs.ImportFromWkt(projref)
        # if srs.IsLocal():
        #     name = srs.GetAttrValue('LOCAL_CS')
        #     srs.SetWellKnownGeogCS('WGS84')
        #     srs.SetProjCS(name)
        return srs
        # return srs.GetAttrValue('PROJECTION')
