#!/usr/bin/python
#coding=utf-8
'''
    project=edu-test,
    name=image_normal.py,
    date=2019/12/27 0027,
    author='zhangxuya',
    author_email='lionhenryzxy@sina.com',
    function：
    calculate the change compare the past
    prerequisite:
        based on Python 3.x
        need Python module
    Any issues or improvements please contact author
'''
import os
import sys
from pyproj import Proj
from  proj_name import name_proj
from datahandler.dataprovider import DataProvider
from datahandler.gtifwriter import GTifWriter
import gdal
import numpy
from PIL import Image
if __name__ == '__main__':
    # path = r'D:\DATA\tile'#sys.argv[1]
    # for dir_root,dir_dirs,dir_files in os .walk(path):
    #     for single in dir_files:
    #         if '.JPG' in single:
    #             singlepath = os.path.join(dir_root,single)
    #             outpath = os.path.join(r'D:\DATA\newtile',single.split('_')[2][1:]+'.JPG')
    #             tempimg = Image.open(singlepath)
    #             tempimg = tempimg.resize((4096,4096),Image.ANTIALIAS)
    #             tempimg.save(outpath)
    #             print 'transform from {}--------->{}'.format(singlepath,outpath)
    singlepath = r"D:\DATA\nctile\FY4A_AGRI_Latlon_L2_VWI_NULL_20191224024500_20191224025959_1000M_POAI_X.NC"
    outpath = r"D:\DATA\nctile\FY4A_AGRI_Latlon_L2_VWI_NULL_20191224024500_20191224025959_1000M_POAI_X.tif"
    handle = DataProvider(singlepath)
    dataset = handle.get_dataset('VWI').astype(numpy.int16)
    proj = name_proj('+lon_0=112 +k=1 +ellps=WGS84 +datum=WGS84 +y_0=0 +errcheck=True +proj=longlat +x_0=0 +units=m +no_defs')
    # desproj = Proj(proj)
    tif_handle = GTifWriter(0.01,[54,70],dataset,proj)
    tif_handle.write(outpath)
    gdal.AllRegister()
    ds = gdal.Open(outpath)
    ds.BuildOverviews(overviewlist=[1, 2, 4, 8, 16, 32])
    print 'transform from {}--------->{}'.format(singlepath,outpath)